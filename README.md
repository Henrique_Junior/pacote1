# pacote1

> Pacote de exemplo

## Instalação

``` bash
composer require henrique/pacote1
```

## API

```php
namespace henrique\pacote1
class Exemplo {
    /**
     * Retorna nome ...
     */
    function nome();
}
```

